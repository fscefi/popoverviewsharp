using System;
using MonoTouch.UIKit;
using System.Drawing;
using PopoverViewSharp;
using MonoTouch.CoreGraphics;

namespace UI.Test
{
    public class TestController : UIViewController, IPopoverViewDelegate
    {
        PopoverView pv
        {
            get;
            set;
        }

        public TestController()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.View.UserInteractionEnabled = true; 

            var tapRecognizer = new UITapGestureRecognizer();           

            tapRecognizer.AddTarget(() => OnViewTapped(tapRecognizer));            
//            tapRecognizer.NumberOfTapsRequired = 1;
//            tapRecognizer.NumberOfTouchesRequired = 1;
            
            this.View.AddGestureRecognizer(tapRecognizer);           

            this.View.BackgroundColor = UIColor.White; 

            var testButton = UIButton.FromType(UIButtonType.RoundedRect);
            testButton.Frame = new RectangleF(100, 100, 100, 30);
            testButton.SetTitle("Click me!", UIControlState.Normal);
            testButton.TouchUpInside += (sender, e) => {
                
                var button = sender as UIButton;
                
                var point = new PointF(button.Frame.GetMidX(), button.Frame.GetMidY());
                
                Console.WriteLine(string.Format("Clicked at {0}, {1}", point.X, point.Y));
                
                PopoverView.ShowPopoverAtPoint(point, this.View, @"DEBUG", new string[] {"click", "from", "button"}, this);
            };
            
            this.View.AddSubview(testButton);
        }

        //[Export("ViewTapSelector")]
        protected void OnViewTapped(UIGestureRecognizer tap)
        {
            var point = tap.LocationInView(this.View);
            
            Console.WriteLine(string.Format("Tapped at {0}, {1}", point.X, point.Y));
            
            pv = PopoverView.ShowPopoverAtPoint(point, this.View, @"DEBUG", new string[]
            {
                "hola",
                "que",
                "tal"
            }, this);
        }
        
        #region IPopoverViewDelegate implementation
        
        public void PopoverViewDidSelectItemAtIndex(PopoverView popoverView, int index)
        {
            Console.WriteLine(string.Format("Popover SelectItemAtIndex {0}", index));
            
            //          //Figure out which string was selected, store in "string"
            //          NSString *string = [kStringArray objectAtIndex:index];
            //          
            //          //Show a success image, with the string from the array
            //          [popoverView showImage:[UIImage imageNamed:@"success"] withMessage:string];
            //          
            //          //Dismiss the PopoverView after 0.5 seconds
            //          [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.5f];
        }
        
        public void PopoverViewDidDismiss(PopoverView popoverView)
        {
            Console.WriteLine("Popover Dismiss");
        }

        //      - (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
        //          if(pv) {
        //              [pv animateRotationToNewPoint:CGPointMake(self.view.frame.size.width*0.5f, self.view.frame.size.height*0.5f) inView:self.view withDuration:duration];
        //          }
        //      }

        public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
           //base.WillAnimateRotation(toInterfaceOrientation, duration);

          if(pv != null) {
                pv.AnimateRotationToNewPoint(new PointF(this.View.Frame.Size.Width*0.5f, this.View.Frame.Size.Height*0.5f), this.View, duration);
          }
        }
        #endregion
    }
}

